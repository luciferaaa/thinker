package org.guess.showcase.qixiu.dao;

import org.guess.core.orm.EntityDao;
import org.guess.showcase.qixiu.model.Rdetail;

/**
 * 详细记录dao
 * @author rguess
 * @version 2014-06-04
 */
public interface RdetailDao extends EntityDao<Rdetail, Long>{
	
}
